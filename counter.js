import {gsap} from 'gsap'

console.clear(); // Start with a clean console on refesh 
gsap.registerPlugin(ScrollToPlugin, ScrollTrigger);

let sections = document.querySelectorAll(".section");
let scrollContainer = document.querySelector(".scrollContainer");

//horizontal scroll
let scrollTween = gsap.to(scrollContainer, {
  x: () => -(scrollContainer.scrollWidth - window.innerWidth),
  ease: "none",
  scrollTrigger: {
    trigger: scrollContainer,
    invalidateOnRefresh: true,
    pin: true,
    scrub: 1,
    end: () => "+=" + scrollContainer.scrollWidth,
  },
});

gsap.utils.toArray(".parallax").forEach(text => {
  gsap.timeline({
    defaults: {ease: "none"},
    scrollTrigger: {
      containerAnimation: scrollTween,
      trigger: text,
      start: "left right",
      end: "left left",
      scrub: true
    }
  })
  .fromTo(text, {x: 250}, {x: -250}, 0)
  // .from(text.nextElementSibling, {scale: 0.8}, 0)
});

gsap.utils.toArray(".scale").forEach(text => {
  gsap.timeline({
    defaults: {ease: "none"},
    scrollTrigger: {
      containerAnimation: scrollTween,
      trigger: text,
      start: "left right",
      end: "left left",
      scrub: true
    }
  })
  .fromTo(text, {scale: 1}, {scale: 1.4}, 0)
  // .from(text.nextElementSibling, {scale: 0.8}, 0)
});


gsap.utils.toArray(".video").forEach(video => {
  
  gsap.timeline({
    defaults: {ease: "none"},
    scrollTrigger: {
      containerAnimation: scrollTween,
      trigger: video,
      start: "-50% 45%",
      end: "150% center",
      markers:true,
      onEnter: () => video.play(),
      onLeave: () => video.pause(),
      onEnterBack: () => video.play(),
      onLeaveBack: () => video.pause(),
    }
  })
});

